const { prefix } = require("../../botconfig.json")
const Datastore = require("nedb");
let userroles = new Datastore({ filename: "././data/roles.db", autoload: true});

module.exports = async (bot, message) => {
if(message.author.bot || message.channel.type === "dm") return;

let args = message.content.slice(prefix.length).trim().split(/ +/g)
let cmd = args.shift().toLowerCase() //Shift all letters to lowercase

//Commands run
if(!message.content.startsWith(prefix)) return;
let commandfile = bot.commands.get(cmd) || bot.commands.get(bot.aliases.get(cmd))
if(commandfile) commandfile.run(bot, message, args, userroles)

}