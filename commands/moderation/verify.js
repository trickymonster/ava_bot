const { RichEmbed } = require("discord.js");
const { verifycolor } = require("../../colors.json");
const deletereturn = (message => message.delete(10000));

module.exports = {
    config: {
        name: "verify",
        description: "Verifies a member!",
        usage: "-verify <@user> <reason>",
        accessibleby: "Members",
        aliases: ["v"]
    },
run: async (bot, message, args) => {

    message.delete();

    if(!message.member.hasPermission("MANAGE_ROLES") || !message.guild.owner) return message.channel.send("You don't have permission to use this command!").then(deletereturn);
    
    if(!message.guild.me.hasPermission("MANAGE_ROLES")) return message.channel.send("I don't have permission to add roles!").then(deletereturn);

    let verified = message.mentions.members.first() || message.guild.members.get(args[0]) || guild.members.get(args[0]);
    if(!verified) return message.channel.send("Please @ or give an ID to verify someone!").then(deletereturn);

    let verifyrole = message.guild.roles.find(r => r.name === "୨ Verified ୧");

    /*if(!verifyrole) {
        try{
            verifyrole = await message.guild.createRole({
                name: "୨ Verified ୧",
                color: "#9af5d3",
                permissions: []
            })
        } catch(err) {
            console.log(err);
        }*/

let vembed = new RichEmbed()
.setColor(verifycolor)
.setAuthor("Verify")
.setThumbnail(verified.user.displayavatarURL)
.setDescription(`${verified} was verified and sent a notice!`)
.setFooter(bot.user.username)

let DMembed = new RichEmbed()
.setColor(verifycolor)
.setAuthor("Verify Info")
.addField("**Action:**", "Verified")
.addField("**Date:**", message.createdAt)
.setDescription("Thank you for verifying!")
.setFooter(bot.user.username)

if(verified.roles.has(verifyrole.id)) return message.channel.send(`${verified.user} is already verified, silly!`).then(deletereturn)

verified.addRole(verifyrole.id).then(() => {
    verified.send(DMembed)
    message.channel.send(vembed)
})

let moderator = message.guild.members.get(message.author.id).displayName

let embed = new RichEmbed()
.setColor(verifycolor)
.setAuthor(`${message.guild.name} Modlogs`, message.guild.iconURL)
.addField("**Action:**", "Verify")
.addField("**User:**", verified.displayName)
.addField("**User ID:**", verified.user.id)
.addField("**Moderator:**", moderator)
.addField("**Date:**", message.createdAt)
.setFooter(bot.user.username)

let sChannel = bot.channels.get("631471651871129610")
sChannel.send(embed)

    }
}