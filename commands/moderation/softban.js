const { RichEmbed } = require("discord.js");
const { orange } = require("../../colors.json");
const deletereturn = (message => message.delete(10000));

module.exports = {
    config: {
        name: "softban",
        description: "Softbans a member!",
        usage: "-softban <@user> <reason>",
        accessibleby: "Members",
        aliases: ["sb"]
    },
run: async (bot, message, args) => {

    message.delete();

    if(!message.member.roles.find(r => r.name === `─ ❥ Mod`)) return message.channel.send("You don't have permission to use this command!").then(deletereturn);
    if(!message.guild.me.hasPermission("BAN_MEMBERS")) return message.channel.send("I don't have permission to softban!").then(deletereturn)

    let sbanee = message.mentions.members.first() || message.guild.members.get(args[0]) || guild.members.id.get(args[0]);
    if(!sbanee) return message.channel.send("Please @ or give an ID softban someone!").then(deletereturn)

    if(sbanee.id === message.author.id) return message.channel.send("You can't ban yourself!").then(deletereturn)
    if(sbanee.highestRole.position >= message.member.highestRole.position) return message.channel.send("You can't softban someone who has the same role or is higher than you!").then(deletereturn)

    let reason = args.slice(1).join(" ");
    if(!reason) return message.channel.send("Please give a reason!").then(deletereturn)

    let DMembed = new RichEmbed()
    .setColor(orange)
    .setAuthor("Softban Info")
    .addField("**Action:**", "Softban")
    .addField("**Reason:**", reason)
    .addField("**Date:**", message.createdAt)
    .setFooter(bot.user.username)

    let moderator = message.guild.members.get(message.author.id).displayName

    let embed = new RichEmbed()
    .setColor(orange)
    .setAuthor(`${message.guild.name} Modlogs`, message.guild.iconURL)
    .addField("**Action:**", "Softban")
    .addField("**User:**", sbanee.displayName)
    .addField("**User ID:**", sbanee.user.id)
    .addField("**Moderator:**", moderator)
    .addField("**Reason:**", reason)
    .addField("**Date:**", message.createdAt)
    .setFooter(bot.user.username)
    
let sChannel = bot.channels.get("631467610101252126")
sChannel.send(embed)

sbanee.send(DMembed).then(() =>
    message.guild.ban(sbanee, {days: 1, reason: reason})).then(() => message.guild.unban(sbanee.id, { reason: "Softban"})).catch(err => console.log(err))

    let Bembed = new RichEmbed()
    .setColor(orange)
    .setThumbnail(sbanee.user.displayAvatarURL)
    .setAuthor("Softban")
    .setDescription(`${sbanee.user} was softbanned for: ${reason} and sent a notice!`)
    .setFooter(bot.user.username)

message.channel.send(Bembed)

    }
}