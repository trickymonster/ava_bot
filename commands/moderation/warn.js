const { RichEmbed } = require("discord.js");
const { orange } = require("../../colors.json");
const Datastore = require("nedb");
let warns = new Datastore({ filename: "././data/warns.db", autoload: true});
const deletereturn = (m => m.delete(10000));

module.exports = {
    config: {
        name: "warn",
        description: "Adds a warning to a user!",
        usage: "-warn <@user> reason",
        accessibleby: "Members",
        aliases: ["w"]
    },
run: async (bot, message, args) => {

    message.delete();

    if(!message.member.hasPermission("MANAGE_ROLES") || !message.guild.owner) return message.channel.send("You don't have permission to use this command!").then(deletereturn)
    if(!message.guild.me.hasPermission("MANAGE_ROLES")) return message.channel.send("I don't have permission to warn members!!").then(deletereturn)
    let wUser = message.mentions.members.first() || message.guild.members.get(args[0]) || guild.members.id.get(args[0]);
    if(!wUser) return message.channel.send("Please @ or give an ID warn someone!");
    if(wUser.highestRole.position >= message.member.highestRole.position) return message.channel.send("You can't warn someone who has the same role or is higher than you!").then(deletereturn)
    let reason = args.slice(1).join(" ");
    if(!reason) return message.channel.send("Please give a reason!").then(deletereturn)



    let newwarn = {
        user: wUser.user.id,
        warning: reason
    }

    warns.insert(newwarn, function(err, newwarn) {
        console.log("Inserted", newwarn.user, "with warning: ", newwarn.warning)
    })

    warns.find({ user: wUser.user.id }, function(err, newwarn) {
        console.log("Found user: ", newwarn.user, "with warnings: ", newwarn.warning)
    });



    let wEmbed = new RichEmbed()
    .setColor(orange)
    .setThumbnail(wUser.user.displayAvatarURL)
    .setAuthor("Warnings")
    .setDescription(`${wUser.user} has been warned!`)
    .addField("Warns: ", newwarn.warning)
    .setFooter(bot.user.username)
        
    message.channel.send(wEmbed)



    let dmEmbed = new RichEmbed()
    .setColor(orange)
    .setAuthor("Warn Info")
    .addField("**Action:**", "Warn")
    .addField("**Reason:**", reason)
    .addField("**Date:**", message.createdAt)
    .setFooter(bot.user.username)

    wUser.send(dmEmbed)



    let moderator = message.guild.members.get(message.author.id).displayName

    let embed = new RichEmbed()
    .setColor(orange)
    .setAuthor(`${message.guild.name} Modlogs`, message.guild.iconURL)
    .addField("**Action:**", "Warn")
    .addField("**User:**", wUser.displayName)
    .addField("**User ID:**", wUser.user.id)
    .addField("**Moderator:**", moderator)
    .addField("**Reason:**", reason)
    .addField("**Date:**", message.createdAt)
    .setFooter(bot.user.username)

    let sChannel = bot.channels.get("631467576995741716")
    //sChannel.send(embed)

    }
}