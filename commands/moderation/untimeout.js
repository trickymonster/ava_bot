const { RichEmbed } = require("discord.js");
const { green } = require("../../colors.json");
const deletereturn = (message => message.delete(10000));

module.exports = {
    config: {
        name: "untimeout",
        description: "Removes a member from timeout!",
        usage: "-untimeout <@user> <reason>",
        accessibleby: "Members",
        aliases: ["um", "uto", "unmute"]
    },
run: async (bot, message, args, userroles) => {

message.delete();

if(!message.member.hasPermission("MANAGE_ROLES") || !message.guild.owner) return message.channel.send("You don't have permission to use this command!").then(deletereturn)

if(!message.guild.me.hasPermission("MANAGE_ROLES")) return message.channel.send("I don't have permission to add roles!").then(deletereturn)

let mutee = message.mentions.members.first() || message.guild.members.get(args[0]) || guild.members.id.get(args[0]);
if(!mutee) return message.channel.send("Please @ or give an ID to remove someone from timeout!").then(deletereturn)

if(mutee.id === message.author.id) return message.channel.send("You can't remove yourself from timeout!").then(deletereturn)
if(mutee.highestRole.position >= message.member.highestRole.position) return message.channel.send("You can't remove someone from timeout who has the same role or is higher than you!").then(deletereturn)

let muterole = message.guild.roles.get("588072686799028234"); //(r => r.name === "─ ❥ Time out chair");

if(!mutee.roles.has(muterole.id)) return message.channel.send(`${mutee.user} isn't in timeout, silly!`).then(deletereturn)

let membed = new RichEmbed()
.setColor(green)
.setThumbnail(mutee.user.displayAvatarURL)
.setAuthor("Untimeout")
.setDescription(`${mutee.user} was removed from timeout!`)
.setFooter(bot.user.username)

let DMembed = new RichEmbed()
.setColor(green)
.setAuthor("Timeout Info")
.addField("**Action:**", "Untimeout")
//.addField("Length:", `${length} minute(s)`)
//.addField("Reason:", reason)
.addField("**Date:**", message.createdAt)
.setFooter(bot.user.username)



userroles.findOne({ user: mutee.user.id }, function(err, savedroles) {
    console.log("Found user", savedroles.user, "with roles:", savedroles.uroles);
    if(!savedroles.user) return message.channel.send("User couldn't be found in the database!");
    mutee.setRoles(savedroles.uroles);
}).catch(err => {
    console.log(err)
})

mutee.send(DMembed)
message.channel.send(membed).then(() => {
    userroles.remove({ user: mutee.user.id }, function(err) {
        console.log("Removed record")
    })
})
.catch(err => {
    console.log(err)
});



let moderator = message.guild.members.get(message.author.id).displayName

let embed = new RichEmbed()
.setColor(green)
.setAuthor(`${message.guild.name} Modlogs`, message.guild.iconURL)
.addField("**Action:**", "Untimeout")
.addField("**User:**", mutee.displayName)
.addField("**User ID:**", mutee.user.id)
.addField("**Moderator:**", moderator)
.addField("**Date:**", message.createdAt)
.setFooter(bot.user.username)

let sChannel = bot.channels.get("673946763778064384")
sChannel.send(embed)

    }
}