const { RichEmbed } = require("discord.js");
const discord = require("discord.js");
const deletereturn = (m => m.delete(60000));

module.exports = {
    config: {
        name: "addrole",
        description: "Adds a role to the server!",
        usage: "-addrole <@user>",
        accessibleby: "Members",
        aliases: ["ar"]
    },
run: async (bot, message, args) => {

    message.delete();

    let rMember = message.mentions.members.first() || message.guild.members.get(args[0]) || guild.members.id.get(args[0]);

    if(!message.member.hasPermission("MANAGE_ROLES")) return message.channel.send("You don't have permission to perform this command!").then(deletereturn)
    if(!message.guild.me.hasPermission("MANAGE_ROLES")) return message.channel.send("I don't have permission to add roles!").then(deletereturn)

    if(message.author.bot) return;
    const filter = m => m.author.id === message.author.id;

        message.channel.send("```Please define the role name!```").then(deletereturn);
        message.channel.awaitMessages(filter, {
            max: 1,
            time: 60000
            })
        .then(collected => {
            let role = collected.first().content;
                message.channel.send("```Please define the hex code (# included)!```").then(deletereturn);
                message.channel.awaitMessages(filter, {
                    max: 1,
                    time: 60000
                    })
                .then(collected => {
                    let hex = collected.first().content;
                        message.channel.send("```Plesae define the length in days! (0 for infinte time)```").then(deletereturn);
                        message.channel.awaitMessages(filter, {
                            max: 1,
                            time: 60000
                            })
                        .then(collected => {
                            let length = collected.first().content;
                                message.channel.send("```Please define the reason!```").then(deletereturn);
                                message.channel.awaitMessages(filter, {
                                    max: 1,
                                    time: 60000
                                    })
                                .then(collected => {
                                    let reason = collected.first().content;
                                    //let [rMember, role, hex, length, reason] = collected.map(r => r.content);

                            const days = (length * 43200000) // 1 day = 86400000ms
                            let customrole = message.guild.roles.find(cr => cr.name === role);
                            message.channel.guild.createRole({
                                name: (`${role}`),
                                color: (`${hex}`),
                                permissions: []
                                })
                            .catch(err => {
                                console.log(err)
                                })
                            .then(function(role) {
                                rMember.addRole(role)
                                    setTimeout(() => {
                                        setTimeout(() => {                            
                                            rMember.removeRole(role)})
                                        }), days;
                                    }), days;
                                    if(customrole)
                                        rMember.addRole(role)

                                        let Rembed = new RichEmbed()
                                        .setColor(hex)
                                        .setThumbnail(rMember.user.displayAvatarURL)
                                        .setAuthor("Custom Role")
                                        .setDescription(`The role, ${role}, has been added to ${rMember.user}.`)
                                        .setFooter(bot.user.username)
                                        
                                        let DMembed = new RichEmbed()
                                        .setColor(hex)
                                        .setAuthor("Custom Role Info")
                                        .addField("**Action:**", "Custom Role Given")
                                        .addField("**Add Role:**", role)
                                        .addField("**Length:**", `for ${length} day(s)`)
                                        .addField("**Reason:**", reason)
                                        .addField("**Date:**", message.createdAt)
                                        .setFooter(bot.user.username)
                                        
                                        
                                        
                                    message.channel.send(Rembed)
                                    rMember.send(DMembed)
                                        
                                    let moderator = message.guild.members.get(message.author.id).displayName
                                        
                                        let embed = new RichEmbed()
                                        .setColor(hex)
                                        .setAuthor(`${message.guild.name} Modlogs`, message.guild.iconURL)
                                        .addField("**Action:**", "Custom Role")
                                        .addField("**User:**", rMember.displayName)
                                        .addField("**User ID:**", rMember.user.id)
                                        .addField("**Moderator:**", moderator)
                                        .addField("**Role:**", role)
                                        .addField("**Color:**", hex)
                                        .addField("**Length:**", `for ${length} day(s)`)
                                        .addField("**Reason:", reason)
                                        .addField("**Date:**", message.createdAt)
                                        .setFooter(bot.user.username)
                                        
                                    let sChannel = bot.channels.get("631467800858460161")
                                    sChannel.send(embed)

                    })
                })
            })
        })
    }
}