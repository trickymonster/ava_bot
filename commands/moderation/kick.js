const { RichEmbed } = require("discord.js");
const { orange } = require("../../colors.json");
const deletereturn = (message => message.delete(5000));

module.exports = {
    config: {
        name: "kick",
        description: "Kicks a member!",
        usage: "-kick <@user> <reason>",
        accessibleby: "Members",
        aliases: ["k"]
    },
run: async (bot, message, args) => {
    
    message.delete()

    if(!message.member.roles.find(r => r.name === `─ ❥ Mod`)) return message.channel.send("You don't have permission to use this command!").then(deletereturn);
    if(!message.guild.me.hasPermission("KICK_MEMBERS")) return message.channel.send("I don't have permission to add kick members!").then(deletereturn)

    let kickee = message.mentions.members.first() || guild.members.id.get(args[0]);
    if(!kickee) return message.channel.send("Please @ or give an ID to kick someone!").then(deletereturn)

    if(kickee.id === message.author.id) return message.channel.send("You can't kick yourself!").then(deletereturn)
    if(kickee.highestRole.position >= message.member.highestRole.position) return message.channel.send("You can't kick someone who has the same role or is higher than you!").then(deletereturn)

    let reason = args.slice(1,2).join(" ");
    if(!reason) return message.channel.send("Please give a reason!").then(deletereturn)

        let DMembed = new RichEmbed()
        .setColor(orange)
        .setAuthor("Kick Info")
        .addField("**Action:**", "Kick")
        .addField("**Reason:**", reason)
        .addField("**Date:**", message.createdAt)
        .setFooter(bot.user.username)
    
kickee.send(DMembed).then(() =>
    kickee.kick(kickee, { reason: reason })).catch(err => console.log(err));

    let Kembed = new RichEmbed()
    .setColor(orange)
    .setThumbnail(kickee.user.displayAvatarURL)
    .setAuthor("Kick")
    .setDescription(`${kickee.user} was kicked for: ${reason} and sent a notice!`)
    .setFooter(bot.user.username)

    message.channel.send(Kembed)
    
    let moderator = message.guild.members.get(message.author.id).displayName

    let embed = new RichEmbed()
    .setColor(orange)
    .setAuthor(`${message.guild.name} Modlogs`, message.guild.iconURL)
    .addField("**Action:**", "Kick")
    .addField("**User:**", kickee.displayName)
    .addField("**User ID:**", kickee.user.id)
    .addField("**Moderator:**", moderator)
    .addField("**Reason:**", reason)
    .addField("**Date:**", message.createdAt)
    .setFooter(bot.user.username)

let sChannel = bot.channels.get("631467610101252126")
sChannel.send(embed)

    }
}