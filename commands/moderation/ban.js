const { RichEmbed } = require("discord.js");
const { red } = require("../../colors.json");
const deletereturn = (message => message.delete(5000));

module.exports = {
    config: {
        name: "ban",
        description: "Bans a member!",
        usage: "-ban <@user> <reason> <length>",
        accessibleby: "Members",
        aliases: ["b"]
    },
run: async (bot, message, args) => {

    message.delete();

    if(!message.member.roles.find(r => r.name === `─ ❥ Mod`)) return message.channel.send("You don't have permission to use this command!").then(deletereturn);
    if(!message.guild.me.hasPermission("BAN_MEMBERS")) return message.channel.send("I don't have permission to ban members!").then(deletereturn);

    let banee = message.mentions.members.first() || message.guild.members.get(args[0]) || guild.members.id.get(args[0]);
    if(!banee) return message.channel.send("Please @ or give an ID to ban someone!").then(deletereturn);

    if(banee.id === message.author.id) return message.channel.send("You can't ban yourself!").then(deletereturn);
    if(banee.highestRole.position >= message.member.highestRole.position) return message.channel.send("You can't ban someone who has the same role or is higher than you!").then(deletereturn);

    let reason = args.slice(1).join(" ");
    if(!reason) return message.channel.send("Please give a reason!").then(deletereturn);

    //let length = args.slice(1,2);
    //if(!length) return message.channel.send("Please include the length of the ban!").then(deletereturn);

    let DMembed = new RichEmbed()
    .setColor(red)
    .setAuthor("Ban Info")
    .addField("**Action:**", "Ban")
    .addField("**Reason:**", reason)
    //.addField("Length:", `${length} day(s)`)
    .addField("**Date:**", message.createdAt)
    .setFooter(bot.user.username)

banee.send(DMembed).then(() =>
    banee.guild.ban(banee, {reason: reason})).catch(err => console.log(err))

    let Bembed = new RichEmbed()
    .setColor(red)
    .setThumbnail(banee.user.displayAvatarURL)
    .setAuthor("Ban")
    .setDescription(`${banee.user} was banned for: ${reason} and sent a notice!`)
    .setFooter(bot.user.username)

    message.channel.send(Bembed)

//setTimeout(function() {
    //banee.removeRole(muterole.id)}, length * 86400000);

    let moderator = message.guild.members.get(message.author.id).displayName

    let embed = new RichEmbed()
    .setColor(red)
    .setAuthor(`${message.guild.name} Modlogs`, message.guild.iconURL)
    .addField("**Action:**", "Ban")
    .addField("**User:**", banee.displayName)
    .addField("**User ID:**", banee.user.id)
    .addField("**Moderator:**", moderator)
    .addField("**Reason:**", reason)
    //.addField("Length:", `${length} day(s)`)
    .addField("**Date:**", message.createdAt)
    .setFooter(bot.user.username)

let sChannel = bot.channels.get("631467610101252126")
sChannel.send(embed)

    }
}