const { RichEmbed } = require("discord.js");
const { green } = require("../../colors.json");
const deletereturn = (message => message.delete(10000));

module.exports = {
    config: {
        name: "unban",
        description: "Unbans a member!",
        usage: "-unban <@user> <reason>",
        accessibleby: "Members",
        aliases: ["ub"]
    },
run: async (bot, message, args) => {

    message.delete();

    if(!message.member.roles.find(r => r.name === `─ ❥ Mod`)) return message.channel.send("You don't have permission to use this command!").then(deletereturn);
    if(!message.guild.me.hasPermission("BAN_MEMBERS")) return message.channel.send("I don't have permission to unban!").then(deletereturn)

    let banee = await bot.fetchUser(args[0]);
    if(!banee) return message.channel.send("Please give an ID unban someone!").then(deletereturn)

    if(banee.id === message.author.id) return message.channel.send("You can't unban yourself!").then(deletereturn)
    //if(banee.highestRole.position >= message.member.highestRole.position) return message.channel.send("You can't ban someone who has the same role or is higher than you!").then(deletereturn)
    
    let baneeexists = await bot.fetchUser(args[0]); //guild.members.id.get(args[0]);
    if(!baneeexists) return message.channel.send("The user isn't in this server!").then(deletereturn);

    let reason = args.slice(1).join(" ");
    if(!reason) return message.channel.send("Please give a reason!").then(deletereturn)

    let Bembed = new RichEmbed()
    .setColor(green)
    .setAuthor("Unban")
    .setDescription(`${banee} was unbanned!`)
    .setFooter(bot.user.username)

    try {
        message.guild.unban(banee, {reason: reason})
        message.channel.send(Bembed)
    } catch(e) {
        console.log(e.message)
    }

    /*banee.send(DMembed)

    let DMembed = new RichEmbed()
    .setColor(green)
    .setAuthor("**Unban Info**")
    .addField("**Action:**", "Unban")
    .addField("**Reason:**", reason)
    .addField("**Date:**", message.createdAt)
    .setFooter(bot.user.username)*/

    let moderator = message.guild.members.get(message.author.id).displayName

    let embed = new RichEmbed()
    .setColor(green)
    .setAuthor(`${message.guild.name} Modlogs`, message.guild.iconURL)
    .addField("**Action:**", "Unban")
    .addField("**User:**", banee.displayName)
    .addField("**User ID:**", banee.id)
    .addField("**Moderator:**", moderator)
    .addField("**Reason:**", reason)
    .addField("**Date:**", message.createdAt)
    .setFooter(bot.user.username)
    
let sChannel = bot.channels.get("631467610101252126")
sChannel.send(embed)

    }
}