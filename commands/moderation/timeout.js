const { RichEmbed } = require("discord.js");
const { orange } = require("../../colors.json");
const deletereturn = (message => message.delete(10000));

module.exports = {
    config: {
        name: "timeout",
        description: "Adds a member to timeout!",
        usage: "-timeout <@user> <reason>",
        accessibleby: "Members",
        aliases: ["to", "m", "mute"]
    },
run: async (bot, message, args, userroles) => {

    message.delete();

    if(!message.member.hasPermission("MANAGE_ROLES") || !message.guild.owner) return message.channel.send("You don't have permission to use this command!").then(deletereturn)
    if(!message.guild.me.hasPermission("MANAGE_ROLES")) return message.channel.send("I don't have permission to put members in timeout!").then(deletereturn)

    let mutee = message.mentions.members.first() || message.guild.members.get(args[0]) || guild.members.id.get(args[0]);
    if(!mutee) return message.channel.send("Please @ or give an ID put someone in timeout!");

    if(mutee.id === message.author.id) return message.channel.send("You can't timeout yourself!").then(deletereturn)
    if(mutee.highestRole.position >= message.member.highestRole.position) return message.channel.send("You can't put someone in timeout who has the same role or is higher than you!").then(deletereturn)

    //let length = args.slice(1,2)
    //if(!length) return message.channel.send("Please include the length of the timeout!").then(deletereturn)

    let reason = args.slice(1).join(" ");
    if(!reason) return message.channel.send("Please give a reason!").then(deletereturn)

    let muterole = message.guild.roles.get("588072686799028234");
    
    if(mutee.roles.has(muterole.id)) return message.channel.send(`${mutee.user} is already in timeout, silly!`).then(deletereturn)

    let DMembed = new RichEmbed()
    .setColor(orange)
    .setAuthor("Timeout Info")
    .addField("**Action:**", "Timeout")
    //.addField("Length:", `${length} minute(s)`)
    .addField("**Reason:**", reason)
    .addField("**Date:**", message.createdAt)
    .setFooter(bot.user.username)

    let Membed = new RichEmbed()
    .setColor(orange)
    .setThumbnail(mutee.user.displayAvatarURL)
    .setAuthor("Timeout")
    .setDescription(`${mutee.user} was put in timeout and sent a notice!`)
    .setFooter(bot.user.username)

    currentroles = mutee.roles.keyArray();

    let savedroles = {
        user: mutee.user.id,
        uroles: currentroles
    };

    userroles.insert(savedroles, function(err, savedroles) {
        console.log("Inserted", savedroles.user, "with roles:", savedroles.uroles)
    });

    mutee.setRoles([muterole.id]).then(() => {
        mutee.send(DMembed)
        message.channel.send(Membed)
    }).catch(err => {
        console.log(err)
        })

//setTimeout(function() {
    //mutee.setRoles(currentroles);
        //}, length * 60000);

    let moderator = message.guild.members.get(message.author.id).displayName

        let embed = new RichEmbed()
        .setColor(orange)
        .setAuthor(`${message.guild.name} Modlogs`, message.guild.iconURL)
        .addField("**Action:**", "Timeout")
        .addField("**User:**", mutee.displayName)
        .addField("**User ID:**", mutee.user.id)
        .addField("**Moderator:**", moderator)
        //.addField("Length:", `${length} minute(s)`)
        .addField("**Reason:**", reason)
        .addField("**Date:**", message.createdAt)
        .setFooter(bot.user.username)

    let sChannel = bot.channels.get("673946763778064384")
    sChannel.send(embed)

    }
}