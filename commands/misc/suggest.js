const { RichEmbed } = require("discord.js");
const { green } = require("../../colors.json");
const Datastore = require("nedb");
let suggestdb = new Datastore({ filename: "././data/suggest.db", autoload: true});
const deletereturn = (message => message.delete(5000));

module.exports = {
    config: {
        name: "suggest",
        description: "Suggest something for the server!",
        usage: "^suggest",
        accessibleby: "Members",
        aliases: ["s"]
    },
run: async (bot, message, args) => {

    message.delete();

    if(!message.member.roles.get("581625492789264444") || !message.guild.owner) return message.channel.send("You don't have permission to use this command!").then(deletereturn)

    //let sMember = message.mentions.members.first() || message.guild.members.get(args[0]) || guild.members.id.get(args[0]);
    
    if(message.author.bot) return;
    const filter = m => m.author.id === message.author.id;

        message.channel.send("```Please type out your suggestion! (You have 3 minutes, or type 'cancel' to cancel.)```").then(deletereturn);
        message.channel.awaitMessages(filter, {
            max: 1,
            time: 180000
            })
        .then(collected => {

            let sMessage = collected.first().content;

            let sUser = message.guild.members.get(message.author.id)

            /*let timeoutd = {
                user: sUser
            };

            if (suggestdb.find({ user: sUser.user.id }, function(err, timeoutd) {
                    console.log("Found user: ", timeoutd.user)
            })) {
                setTimeout(function() {
                    suggestdb.remove({ user: sUser });
                }, 3600000);
                return "You must wait an hour before you send another suggestion!"
            } else {
                suggestdb.insert(timeoutd, function(err, timeoutd) {
                    console.log("Inserted", timeoutd)
                })
            }
    
            suggestdb.insert(timeoutd, function(err, timeoutd) {
                console.log("Inserted", timeoutd)
            });*/

            let sEmbed = new RichEmbed()
            .setColor(green)
            .setThumbnail(message.member.user.displayAvatarURL)
            .setAuthor("Suggestion")
            .addField("**Suggestion:**", sMessage)
            .addField("**Date:**", message.createdAt)
            .setFooter(bot.user.username);
            
            let sChannel = bot.channels.get("631469137557979137");
            sChannel.send(sEmbed);

        })
    }
}