const { RichEmbed } = require("discord.js");
const { blue } = require("../../colors.json");
const deletereturn = (message => message.delete(120000));

module.exports = {
    config: {
        name: "userInfo",
        description: "Inspects a member!",
        usage: "-userinfo",
        accessibleby: "Members",
        aliases: ["ui"]
    },
run: async (bot, message, args) => {

    message.delete();

    let person = message.mentions.members.first() || message.guild.members.get(args[0]);

    /*let filter = (e => e.id === "579661984828620810");
    let currentroles = person.roles.map(r => r.name, filter);*/

    let uEmbed = new RichEmbed()
    .setColor(blue)
    .setThumbnail(person.user.displayAvatarURL)
    .setAuthor(`${person.nickname}`)
    .addField("**User Name:**", `${person.user}`)
    .addField("**ID:**", `${person.user.id}`)
    //.addField("**Roles:**", `${currentroles}`)
    .addField("**Status:**", `${person.user.presence.status}`)
    .addField("**Created On:**", `${person.user.createdAt}`)
    .setFooter(bot.user.username);

    message.channel.send(uEmbed).then(deletereturn);
    }
}