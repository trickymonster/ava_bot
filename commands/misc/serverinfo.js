const Discord = require("discord.js");
const botconfig = require("../../botconfig.json");
const colors = require("../../colors.json");

module.exports = {
    config: {
        name: "serverInfo",
        description: "Gives all the server info!",
        usage: "-serverinfo",
        accessibleby: "Members",
        aliases: ["si"]
    },
run: async (bot, message, args) => {

    message.delete();

    let owner = await bot.fetchUser("658770126891384866");

    let sEmbed = new Discord.RichEmbed()
    .setColor(colors.blue)
    .setThumbnail(message.guild.iconURL)
    .setAuthor(bot.user.username, bot.user.displayAvatarURL)
    .addField("**Server Name:**", `${message.guild.name}`, true)
    .addField("**Server Owner:**", owner, true)
    .addField("**Member Count:**", `${message.guild.memberCount}`, true)
    .setFooter(bot.user.username);
    message.channel.send(sEmbed);
    }
};