const { token } = require("./botconfig.json");
const { Client, Collection } = require("discord.js");
const bot = new Client();
const { RichEmbed } = require("discord.js");
const { pink_light } = require("./colors.json");

["aliases", "commands"].forEach(x => bot[x] = new Collection());
['console', "commands", "event"].forEach(x => require(`./handlers/${x}`)(bot));

process.on(`unhandledRejection`, error => {
    console.error(`Unhandled promise rejection`, error); 
});

/*bot.on("guildMemberAdd", member => {

    let welcomes = [a, b, c]

   var a = `Whips and chains excite me, but ${member} excites me more!`
   var b = `Hey ${member}, here's your complimentary bondage rope!`
   var c = `Roses are red, violets are blue. Welcome ${member}, now lick my boot!`



    let rMessage = welcomes[Math.floor(Math.random() * welcomes.length)];

    let wembed = new RichEmbed()
    .setColor(pink_light)
    .setThumbnail(guild.iconURL)
    .setAuthor(bot.user.username)
    .addField(rMessage)

    member.guild.channels.get("675388396470009870").send(wembed)
    })*/

bot.on("error", (e) => console.error(e));
bot.on("warn", (e) => console.warn(e));
bot.on("debug", (e) => console.info(e));

bot.login(token);